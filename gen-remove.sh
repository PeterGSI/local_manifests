#!/usr/bin/env bash

REMOVE_PREFIXES=("device/google" "prebuilts/android-emulator")

echo '<?xml version="1.0" encoding="UTF-8"?>'
echo '<manifest>'

for prefix in "${REMOVE_PREFIXES[@]}"; do
    cat ../manifests/default.xml | grep path=\"$prefix | sed -r "s@^.*path=\"($prefix[^\"]*)\".*@\1@" | while read line; do
        if [[ "$line" =~ (.*)cuttlefish(.*) ]]; then
            continue
	fi
        echo "  <remove-project path=\"$line\" />"
    done
done

echo '</manifest>'
